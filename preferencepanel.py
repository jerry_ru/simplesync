#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os.path

import wx

from outwiker.gui.preferences.configelements import StringElement, BooleanElement
from outwiker.core.system import getOS

from .simplesyncconfig import SimpleSyncConfig


class PreferencePanel (wx.Panel):
    """
    Панель с настройками
    """
    def __init__ (self, parent, config, lang):
        """
        parent - родитель панели (должен быть wx.Treebook)
        config - настройки из plugin._application.config
        lang - функция для локализации, созданная с помощью plugin._init_i18n
        """
        wx.Panel.__init__ (self, parent, style=wx.TAB_TRAVERSAL)

        self._ = lang

        self.__createGui()
        self.__controller = PrefPanelController (self, config)


    def __createGui(self):
        """
        Создать элементы управления
        """
        mainSizer = wx.FlexGridSizer (2, 1)
        mainSizer.AddGrowableCol(0)
        mainSizer.AddGrowableCol(1)

        self.__layout (mainSizer)
        self.SetSizer(mainSizer)


    def __layout (self, mainSizer):
        """
        Создать интерфейс, связанный с языком программирования по умолчанию
        """
        lable = wx.StaticText(self, -1, self._(u"sync options"))
        self.enableBox = wx.CheckBox(self, -1, _(u"Enable Sync script"))
        self.scriptLabel = wx.TextCtrl (self, -1, _(u"Sync script"))

        mainSizer.Add (
                lable, 
                proportion=1,
                flag = wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                border=2
                )

        mainSizer.Add (self.enableBox,
                proportion=1,
                flag = wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                border=2
                )

        mainSizer.Add (self.scriptLabel,
                proportion=1,
                flag = wx.ALL | wx.EXPAND,
                border=2
                )


    def LoadState(self):
        self.__controller.loadState()


    def Save (self):
        self.__controller.save()



class PrefPanelController (object):
    """
    Контроллер для панели настроек
    """
    def __init__ (self, owner, config):
        self.__owner = owner
        self.__config = SimpleSyncConfig(config)

    def loadState (self):
        self.__enableScriptElement = BooleanElement(self.__config.enableScript, self.__owner.enableBox)         
        self.__syncScriptElement = StringElement(self.__config.syncScript, self.__owner.scriptLabel) 


    def save (self):
        self.__config.enableScript.value = self.__owner.enableBox.GetValue()
        self.__config.syncScript.value = self.__owner.scriptLabel.GetValue()
