#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os.path
import sys

from outwiker.core.pluginbase import Plugin
from outwiker.core.system import getOS
from outwiker.core.commands import getCurrentVersion
from outwiker.core.version import Version, StatusSet
from outwiker.gui.preferences.preferencepanelinfo import PreferencePanelInfo

from .simplesyncconfig import SimpleSyncConfig


class PluginSimpleSync (Plugin):
    """
    Плагин, добавляющий выполнение синхронизации при закрытии вики
    """
    def __init__ (self, application):
        """
        application - экземпляр класса core.application.ApplicationParams
        """
        Plugin.__init__ (self, application)
        self.__version = u"0.1"
        self.SOURCE_SYNC_ID = u"PLUGIN_SIMPLESYNC_TOOL_ID"


    def initialize(self):
        self._application.onPreferencesDialogCreate += self.__onPreferencesDialogCreate
        self._application.onWikiClose += self.__onClose
        self._application.onWikiOpen += self.__onClose


    def __onPreferencesDialogCreate (self, dialog):
        from .preferencepanel import PreferencePanel
        prefPanel = PreferencePanel (dialog.treeBook, self._application.config, _)

        panelName = _(u"SimpleSync [Plugin]")
        panelsList = [PreferencePanelInfo (prefPanel, panelName)]
        dialog.appendPreferenceGroup (panelName, panelsList)

    def __onClose (self, root):
        if root == None: return
        
        import subprocess
        import time
            
        SimpleSyncConfig
        
        cmd = self.config.syncScript.value
        print "Executing:", cmd
        
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True) 
        out, err = p.communicate()        

        result = p.poll()
        
        print "out:"
        print out
        print "----"
        
        if result:
            print "error:"
            print err
            
        print root.path


    @property
    def config (self):
        return SimpleSyncConfig (self._application.config)


    @property
    def name (self):
        return u"Sync"


    @property
    def description (self):
        return _(u"""Simple Sync Script. Example: cd /tmp/1123/ && git add . && git commit -am "commit" && git push """)


    @property
    def version (self):
        return self.__version


    def destroy (self):
        """
        Уничтожение (выгрузка) плагина. Здесь плагин должен отписаться от всех событий
        """
        self._application.onPreferencesDialogCreate -= self.__onPreferencesDialogCreate
        self._application.onWikiClose -= self.__onClose

