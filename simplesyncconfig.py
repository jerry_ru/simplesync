#!/usr/bin/python
# -*- coding: UTF-8 -*-

from outwiker.core.config import StringOption, BooleanOption


class SimpleSyncConfig (object):
    def __init__ (self, config):
        self.__config = config

        section = u"SimpleSyncPlugin"

        # Скрипт по умолчанию
        self.DEFAULT_SCRIPT = u""
        self.__syncScript = StringOption(self.__config, section, u"SimpleSyncScript", self.DEFAULT_SCRIPT)

        # Скрипт по умолчанию
        self.DEFAULT_ENABLE = False
        self.__enableScript = BooleanOption(self.__config, section, u"EnableSyncScript", self.DEFAULT_ENABLE)

    @property
    def syncScript (self):
        return self.__syncScript

    @property
    def enableScript (self):
        return self.__enableScript
